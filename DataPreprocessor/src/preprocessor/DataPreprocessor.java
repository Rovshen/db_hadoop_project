package preprocessor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.StringTokenizer;

public class DataPreprocessor {
    
    private RandomAccessFile fileToAccess = null;
    private FileChannel      inChannel    = null;
    private InputStream      inStr        = null;
    private BufferedReader   bfReader     = null;
    private File             outFile      = null;
    private FileWriter       fileWr       = null;
    private BufferedWriter   buffWr;
    
    public DataPreprocessor() {
    
    }
    
    /* to change default jre sudo update-alternatives --config java */
    public static void main(String[] args) {
    
        /**
         * java jar DataPreprocessor.jar
         * /graphData/inputData/outFileSampleDataSpace.txt
         * /graphData/outputData/folderProducId1 2>&1 > ProductList_logfile.txt
         * &
         */
        
        DataPreprocessor process = new DataPreprocessor();
        if (args.length != 2) {
            System.out
                    .println("usage: java -jar DataPreprocessor.jar <file to process> <output file>");
            System.exit(1);
        }
        
        try {
            process.prepareOutFile(args);
            process.readWriteData(args);
        } catch (IOException e2) {
            e2.printStackTrace();
            
            try {
                process.closeStreams();
                
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            
        } finally {
            
            try {
                process.closeStreams();
            } catch (IOException e) {
                e.printStackTrace();
            }
            
        }// end finally
        
    }// end of main
    
    /**
     * 
     * @param process
     * @param args
     * @throws IOException
     */
    public void prepareOutFile(String args[])
            throws IOException {
    
        outFile = new File(args[1]);
        
        if (outFile.exists()) {
            
            /* clear the file */
            outFile.createNewFile();
        } else {
            /* clear the file */
            outFile.createNewFile();
        }
        
        fileWr = new FileWriter(outFile, true);
        
        /* for better performance */
        buffWr = new BufferedWriter(fileWr);
    }// end of file prep
    
    /**
     * 
     * @param args
     * @throws IOException
     */
    public void readWriteData(String args[]) throws IOException {
    
        fileToAccess = new RandomAccessFile(args[0], "r");
        inChannel = fileToAccess.getChannel();
        Long fileSize = inChannel.size();
        System.out.println("File size in bytes: " + fileSize);
        /* read 64MB chunks
         * http://www.matisse.net/bitcalc/?input_amount=64&input_units=megabytes&
         * notation=legacy
         * conversion results
         * 
         * Running time: 579.597 - seconds.
         * 34737424 - records processed */
        
        ByteBuffer buffer = ByteBuffer.allocate(9277216);
        int counter = 0;
        long startTime = System.currentTimeMillis();
        
        while (inChannel.read(buffer) > 0) {
            buffer.flip();
            
            if (buffer.hasArray()) {
                inStr = new ByteArrayInputStream(buffer.array());
            } else {
                System.out.println("no byte array in the buffers");
            }
            bfReader = new BufferedReader(new InputStreamReader(
                    inStr));
            String temp = null;
            
            while ((temp = bfReader.readLine()) != null) {
                
                StringTokenizer st = new StringTokenizer(temp, ":");
                // Loop through the tokens and see if they match the word
                // specified by the user.
                while (st.hasMoreTokens()) {
                    
                    String word = st.nextToken().trim();
                    // System.out.println(word);
                    if (st.hasMoreTokens()) {
                        if (word.equals("product/productId")) {
                            String wordToReqcord = st.nextToken().trim();
                            buffWr.append(wordToReqcord + ",");
                        } else if (word.equals("review/userId")) {
                            String wordToReqcord = st.nextToken().trim();
                            buffWr.append(wordToReqcord + ",");
                        } else if (word.equals("review/helpfulness")) {
                            String wordToReqcord = st.nextToken().trim();
                            buffWr.append(wordToReqcord + ",");
                        } else if (word.equals("review/score")) {
                            String wordToReqcord = st.nextToken().trim();
                            buffWr.append(wordToReqcord);
                            counter++;
                            buffWr.append("\n");
                        }
                    }
                    
                }
                
            }
            buffer.clear();
            
        }// end of reading file
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        double totalTimeD = totalTime / 1000.0;
        System.out.println("Running time: " + totalTimeD + " - seconds.");
        System.out.println(Integer.toString(counter));
        
        // clear the buffer
        buffWr.flush();
        
    }// end of read
    
    public void closeStreams() throws IOException {
    
        inStr.close();
        bfReader.close();
        inChannel.close();
        fileToAccess.close();
    }
    
    /* A method which converts a string into a char array, and then converts it
     * into its ascii representation */
    // not used at the moment as the numbers produced do not fit into the long
    // value
    @SuppressWarnings("unused")
    private static String hashFunction(String uIdOrPid) {
    
        char[] charArray = uIdOrPid.toCharArray();
        StringBuilder build = new StringBuilder();
        for (int i = 0; i < charArray.length; i++) {
            long ascii = charArray[i];
            String intToString = "" + ascii;
            build.append(intToString);
        }
        return build.toString();
    }
    
}// end of class
