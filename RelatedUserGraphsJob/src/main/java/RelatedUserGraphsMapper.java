import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

public class RelatedUserGraphsMapper extends
        Mapper<Text, Text, Text,
        Text> {
    
    private String                      queryUsersList;
    private MultipleOutputs<Text, Text> mos;
    
    // into the mapper
    @Override
    protected void setup(Context context) throws IOException,
            InterruptedException {
    
        Configuration conf = context.getConfiguration();
        queryUsersList = conf.get("queryUserIDList");
        mos = new MultipleOutputs<Text, Text>(context);
    }
    
    /**
     * Map UsIDs as a key associated with PrIDs list
     */
    @Override
    public void map(Text userIdkey, Text productsListvalue, Context context)
            throws IOException, InterruptedException {
    
        // query user -> 325353 rel users
        // ->;325454;325757;325656;325555;3249484948
        String[] queryUsersListArr = queryUsersList.split(";");
        Set<Long> relatedUsersSet = new HashSet<Long>();
        for (int i = 0; i < queryUsersListArr.length; i++) {
            if (!queryUsersListArr[i].trim().equals("")) {
                relatedUsersSet.add(Long.parseLong(queryUsersListArr[i]));
            }
        }
        
        /** 1st Root Us ID, 2nd and rest Pr */
        String[] prodStrArr = null;
        String prodListStr = productsListvalue.toString();
        if (prodListStr.contains(" ")) {
            prodStrArr = prodListStr.split(" ");
        } else {
            prodStrArr = new String[1];
            prodStrArr[0] = prodListStr;
        }
        int numberOfProducts = prodStrArr.length;
        
        Set<Long> uniqueProductIds = new HashSet<Long>();
        // we expect at least key value on the line
        String usID = userIdkey.toString().trim();
        Long userIdLong = Long.parseLong(usID);
        String queriedUserIdStr = queryUsersListArr[1].trim();
        
        if (numberOfProducts > 0) {
            
            Long productId;
            String currPrStr;
            if (relatedUsersSet.contains(userIdLong)) {
                for (int i = 0; i < numberOfProducts; i++) {
                    currPrStr = prodStrArr[i].trim();
                    if (!currPrStr.equals("")) {
                        productId = Long.parseLong(currPrStr);
                        uniqueProductIds.add(productId);
                        productId = null;
                        currPrStr = null;
                    }
                }
                int prSize = uniqueProductIds.size();
                if (prSize > 1 || (usID.equals(queriedUserIdStr) && prSize > 0)) {
                    StringBuilder build = new StringBuilder();
                    for (Long productKey : uniqueProductIds) {
                        build.append(usID).append("\t")
                                .append(productKey.toString())
                                .append("\r\n");
                    }
                    // build.append(" testing query user id ")
                    // .append(queriedUserIdStr)
                    // .append("  user id ")
                    // .append(usID);
                    productsListvalue.set(build.toString());
                    mos.write(productsListvalue, new Text(""),
                            userIdLong.toString());
                }// record related user graphs if only related by more than one
                 // product
            }
        }
    }// end map
    
    @Override
    public void cleanup(Context cont) throws IOException, InterruptedException {
    
        mos.close();
    }
}
