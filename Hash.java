import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Hash {
	

	public static void main(String[] args) {
		
		
		String inputFilePath = null, outputFilePath = null;
        // Checks for the correct number of arguments
		if (args.length < 2 || args.length > 2) { 
			
			System.err.println("Wrong number of Arguments!");
			System.exit(1);
			
		}
		else {
			try {
			inputFilePath = args[0]; // First command line argument is the input file path
			outputFilePath = args[1]; // Second command line argument is the output file path
					
		    BufferedReader in = new BufferedReader(new FileReader(inputFilePath));
		    File file = new File(outputFilePath);
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
	    	BufferedWriter bw = new BufferedWriter(fw);
			// if file does not exists, then create it
	    	if (!file.exists()) {
	    		file.createNewFile();
	    	}
		    String str;
		    System.out.println("Reading from file...Writing to file");
		    while ((str = in.readLine()) != null) {
		    	String[] line=str.split("\t");
		        String pId = hashFunction(line[0]);
		        bw.write(pId+"\t");
		        String[] uIds = line[1].split(",");
		                
		        for (int j = 0 ; j < uIds.length; j++) {
		        	String userId = hashFunction(uIds[j]);
		            try {
		            	if(j < uIds.length - 1) {
		            		bw.write(userId+",");
		                    }
		               
		            	else{
		                	bw.write(userId);
		                   }
		                    	
		                		
		                } catch (IOException e) {
		                	 e.printStackTrace();
		                	}

		                	}
		                bw.newLine();
		                }
		            in.close();
		            System.out.println("Process is complete!");
		            
		            
		        }
			   catch (IOException e) {
		            System.out.println(e.getMessage());
		        }
			
			
		}
		
		
			
            
		
		
		
		    
		

		
		
	}
	
   /*
    * A method which converts a string into a char array, and then converts it into its ascii representation
    */
   private static String hashFunction(String uIdOrPid) {
	
			char[] charArray = uIdOrPid.toCharArray();
			String hashResult = "";
			
			for(int i = 0 ; i < charArray.length; i++) {
				long ascii = (int) charArray[i];
				String intToString = "" + ascii;
				hashResult += intToString;

				}
			
			return hashResult;
   }
   
   
   
}



