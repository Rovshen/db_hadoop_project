package indexingWithPreservedRatingGL;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author Rovshen
 * 
 * Row data to Graphlab acceptable format for Collabarative filtering with
 * indexing
 * 
 */
public class IndexingWithPreservedRatingGL {
    
    private RandomAccessFile fileToAccess    = null;
    private FileChannel      inChannel       = null;
    private InputStream      inStr           = null;
    private BufferedReader   bfReader        = null;
    private File             outFileUsIndex  = null;
    private File             outFilePrIndex  = null;
    private File             outFilePrUsData = null;
    private FileWriter       fileWrUsId      = null;
    private BufferedWriter   buffWrUsId;
    private BufferedWriter   buffWrPrUsData;
    private FileWriter       fileWrPrId      = null;
    private FileWriter       fileWrPrUsData  = null;
    private BufferedWriter   buffWrPrId;
    
    public IndexingWithPreservedRatingGL() {
    
    }
    
    /* to change default jre sudo update-alternatives --config java */
    public static void main(String[] args) {
    
        /**
         * java jar DataPreprocessor.jar
         * /graphData/inputData/outFileSampleDataSpace.txt
         * /graphData/outputData/folderProducId1 2>&1 > ProductList_logfile.txt
         * &
         */
        
        IndexingWithPreservedRatingGL process = new IndexingWithPreservedRatingGL();
        if (args.length != 4) {
            System.out
                    .println("usage: java -jar IndexingWithPreservedRatingGL.jar <file to process> <output file product id index><out file user id index><out file product user data>");
            System.exit(1);
        }
        
        try {
            process.prepareOutFile(args);
            process.readWriteData(args);
        } catch (IOException e2) {
            e2.printStackTrace();
            
            try {
                process.closeStreams();
                
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            
        } finally {
            
            try {
                process.closeStreams();
            } catch (IOException e) {
                e.printStackTrace();
            }
            
        }// end finally
        
    }// end of main
    
    /**
     * 
     * @param process
     * @param args
     * @throws IOException
     */
    public void prepareOutFile(String args[])
            throws IOException {
    
        outFileUsIndex = new File(args[2]);
        outFilePrIndex = new File(args[1]);
        outFilePrUsData = new File(args[3]);
        
        if (outFileUsIndex.exists()) {
            
            /* clear the file */
            outFileUsIndex.createNewFile();
        } else {
            /* clear the file */
            outFileUsIndex.createNewFile();
        }
        if (outFilePrIndex.exists()) {
            
            /* clear the file */
            outFilePrIndex.createNewFile();
        } else {
            /* clear the file */
            outFilePrIndex.createNewFile();
        }
        if (outFilePrUsData.exists()) {
            
            /* clear the file */
            outFilePrUsData.createNewFile();
        } else {
            /* clear the file */
            outFilePrUsData.createNewFile();
        }
        
        fileWrUsId = new FileWriter(outFileUsIndex, true);
        fileWrPrId = new FileWriter(outFilePrIndex, true);
        fileWrPrUsData = new FileWriter(outFilePrUsData, true);
        
        /* for better performance */
        buffWrUsId = new BufferedWriter(fileWrUsId);
        buffWrPrUsData = new BufferedWriter(fileWrPrUsData);
        buffWrPrId = new BufferedWriter(fileWrPrId);
    }// end of file prep
    
    /**
     * 
     * @param args
     * @throws IOException
     */
    public void readWriteData(String args[]) throws IOException {
    
        fileToAccess = new RandomAccessFile(args[0], "r");
        inChannel = fileToAccess.getChannel();
        Long fileSize = inChannel.size();
        System.out.println("File size in bytes: " + fileSize);
        
        ByteBuffer buffer = ByteBuffer.allocate(9277216);
        long startTime = System.currentTimeMillis();
        ArrayList<String> productUserArr = new ArrayList<String>();
        ArrayList<String> productUserArrIndexed = new ArrayList<String>();
        Map<String, Long> productUniqMap = new HashMap<String, Long>();
        Map<String, Long> userUniqMap = new HashMap<String, Long>();
        Set<String> usersUniq = new HashSet<String>();
        Set<String> productsUniq = new HashSet<String>();
        
        while (inChannel.read(buffer) > 0) {
            buffer.flip();
            
            if (buffer.hasArray()) {
                inStr = new ByteArrayInputStream(buffer.array());
            } else {
                System.out.println("no byte array in the buffers");
            }
            bfReader = new BufferedReader(new InputStreamReader(
                    inStr));
            String temp = null;
            
            while ((temp = bfReader.readLine()) != null) {
                
                String[] strArr = temp.split(",");
                // Loop through the tokens and see if they match the word
                // specified by the user.
                Double rating = null;
                if (strArr.length > 3) {
                    String productId = strArr[0].trim();
                    String UserId = strArr[1].trim();
                    try {
                        rating = Double.parseDouble(strArr[3]);
                    } catch (NumberFormatException e) {
                    }
                    if (rating >= 4.0) {
                        if (!productId.equals("unknown")
                                && !UserId.equals("unknown") && rating != null) {
                            usersUniq.add(UserId);
                            productsUniq.add(productId);
                            productUserArr.add(productId + ", " + UserId + ","
                                    + rating.toString());
                        }
                    }
                }
            }
            buffer.clear();
            
        }// end of reading file
        Long indexUs = 1l;
        for (String uniqUs : usersUniq) {
            userUniqMap.put(uniqUs, indexUs);
            indexUs++;
        }
        
        Long indexPr = 1l;
        for (String uniqPr : productsUniq) {
            productUniqMap.put(uniqPr, indexPr);
            indexPr++;
        }
        
        for (int i = 0; i < productUserArr.size(); i++) {
            
            String[] prodUsArr = productUserArr.get(i).split(",");
            String productId = prodUsArr[0].trim();
            String userId = prodUsArr[1].trim();
            String ratingStr = prodUsArr[2].trim();
            Long productIndex = productUniqMap.get(productId);
            Long userIdex = userUniqMap.get(userId);
            if (productIndex != null && userIdex != null && ratingStr != "") {
                productUserArrIndexed.add(productIndex + " " + userIdex + " "
                        + ratingStr);
            }
        }
        
        // record product string product index
        for (String prodId : productUniqMap.keySet()) {
            buffWrPrId.append(String.valueOf(productUniqMap.get(prodId)))
                    .append(" ")
                    .append(prodId).append("\n");
        }
        buffWrPrId.flush();
        // record user string user index table
        for (String userId : userUniqMap.keySet()) {
            buffWrUsId.append(String.valueOf(userUniqMap.get(userId)))
                    .append(" ")
                    .append(userId);
            buffWrUsId.append("\n");
        }
        buffWrUsId.flush();
        
        // record initial data
        for (int i = 0; i < productUserArrIndexed.size(); i++) {
            buffWrPrUsData
                    .append(productUserArrIndexed.get(i)).append("\n");
        }
        buffWrPrUsData.flush();
        
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        double totalTimeD = totalTime / 1000.0;
        System.out.println("Running time: " + totalTimeD + " - seconds.");
        System.out.println("productUniqMap " + productUniqMap.size()
                + " userUniqMap " + userUniqMap.size()
                + " productUserMapIndexed " + productUserArrIndexed.size());
        
    }// end of read
    
    public void closeStreams() throws IOException {
    
        inStr.close();
        bfReader.close();
        inChannel.close();
        fileToAccess.close();
        
        buffWrUsId.close();
        buffWrPrId.close();
        buffWrPrUsData.close();
    }
    
}// end of class
