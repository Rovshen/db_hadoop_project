import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class RelatedUserListJob extends Configured implements Tool {
    
    public static void main(String[] args) throws Exception {
    
        /**
         * Get the related users for the query user based on the bipartite graph
         * optimization - process UsId.PrID list to get related user's vectors
         */
        
        int res = ToolRunner.run(new RelatedUserListJob(), args);
        System.exit(res);
        
    }
    
    public int run(String[] args) throws Exception {
    
        if (args.length != 3) {
            System.err
                    .println("Usage: hadoop -jar RelatedUserListJob.jar <user id> <bipartite graph> <output folder>");
            System.exit(1);
        }
        // Path usIdInputPath = new Path(args[0]);
        Path inputPath = new Path(args[1]);
        Path outputPath = new Path(args[2]);
        Configuration conf = new Configuration();
        // get the query user id
        conf.set("queryUserID", args[0]);
        conf.set("mapreduce.output.textoutputformat.separator", ";");
        Job job = Job.getInstance(conf);
        
        job.setJar("RelatedUserListJob.jar");
        job.setJobName("RelatedUserListJob");
        
        /**
         * Also a small tip for you, you can separate the files with comma ","
         * so you can set them with a single call like this: hadoop jar
         * capital.jar org.myorg.Capital
         * /user/cloudera/capital/input/City.dat,/user
         * /cloudera/capital/input/Country.dat And in your java
         * code:FileInputFormat.addInputPaths(job, args[1]);
         * */
        
        // Set up the input
        job.setInputFormatClass(KeyValueTextInputFormat.class);
        KeyValueTextInputFormat.addInputPath(job, inputPath);
        // Mapper
        job.setMapperClass(RelatedUserListMapper.class);
        
        /* MultipleInputs.addInputPath(job, usIdInputPath,
         * TextInputFormat.class, GraphBuilderMapperUsId.class);
         * 
         * MultipleInputs.addInputPath(job, prIdInputPath,
         * TextInputFormat.class,
         * GraphBuilderMapperPrId.class); */
        
        // Reducer
        job.setNumReduceTasks(0);
        
        // Output
        job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job, outputPath);
        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(Text.class);
        
        // Execute
        boolean res = job.waitForCompletion(true);
        if (res) {
            return 0;
        } else {
            return 1;
        }
    }
}
