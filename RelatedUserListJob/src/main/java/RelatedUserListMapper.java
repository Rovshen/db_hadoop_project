import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class RelatedUserListMapper extends
        Mapper<Text, Text, LongWritable,
        Text> {
    
    private Long queryUserID;
    LongWritable keyOut = new LongWritable();
    
    // into the mapper
    @Override
    protected void setup(Context context) throws IOException,
            InterruptedException {
    
        Configuration conf = context.getConfiguration();
        try {
            queryUserID = Long.parseLong(conf.get("queryUserID"));
        } catch (NumberFormatException e) {
        }
        
    }
    
    /**
     * Map UsID as a key associated with PrID and set value to
     * ";" PrID ":" Pr.UsIDs
     */
    @Override
    public void map(Text key, Text value, Context context)
            throws IOException, InterruptedException {
    
        if (queryUserID != null) {
            /** 1st Root Us ID, 2nd and rest Pr -> UsIDs map */
            String[] prUsersAdjList = value.toString().trim().split(";");
            int numbOfAdjLists = prUsersAdjList.length;
            // we expect at least key value on the line
            if (numbOfAdjLists > 1) {
                Long userIdStr = Long.parseLong(key.toString());
                if (queryUserID.equals(userIdStr)) {
                    Set<Long> uniqueRelatedUsers = new HashSet<Long>();
                    String[] productIdUserAdjListElem;
                    Long relatedUser;
                    for (int i = 0; i < numbOfAdjLists; i++) {
                        
                        productIdUserAdjListElem = prUsersAdjList[i].split(" ");
                        // skip the product id from adj list
                        for (int j = 1; j < productIdUserAdjListElem.length; j++) {
                            relatedUser = Long
                                    .parseLong(productIdUserAdjListElem[j]
                                            .trim());
                            if (!relatedUser.equals(userIdStr)) {
                                uniqueRelatedUsers.add(relatedUser);
                            }
                        }
                    }
                    
                    StringBuilder build = new StringBuilder();
                    build.append(userIdStr.toString());
                    
                    for (Long userKey : uniqueRelatedUsers) {
                        build.append(";").append(userKey.toString());
                    }
                    
                    keyOut.set(userIdStr);
                    context.write(keyOut, new Text(build.toString()));
                }
            }// end if 2 or more
        }
        
    }// end map
}// end class
