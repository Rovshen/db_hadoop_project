import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class GraphBuilderMapperPrId extends
        Mapper<Text, Text, LongWritable,
        Text> {
    
    private final LongWritable UsID              = new LongWritable();
    private String             queryUserID;
    Text                       usersListValueOut = new Text();
    
    // into the mapper
    @Override
    protected void setup(Context context) throws IOException,
            InterruptedException {
    
        Configuration conf = context.getConfiguration();
        try {
            queryUserID = conf.get("queryUserID");
        } catch (NumberFormatException e) {
        }
    }
    
    /**
     * Map UsID as a key associated with PrID and set value to
     * ";" PrID ":" Pr.UsIDs
     */
    @Override
    public void map(Text prodKey, Text usersListValue, Context context)
            throws IOException, InterruptedException {
    
        if (queryUserID != null && !queryUserID.equals(0)) {
            /** 1st Pr ID, 2nd and rest UsID */
            String[] usersStrArr = usersListValue.toString().trim().split(" ");
            /**
             * save current product id user ids key value pair as one value to
             * use later in reducer
             */
            int lineLength = usersStrArr.length;
            // we expect at least key value on the line
            if (lineLength > 1) {
                // safe product id as 1st list element
                StringBuilder build = new StringBuilder(prodKey.toString());
                build.append(" ").append(usersListValue.toString().trim());
                usersListValueOut.set(build.toString());
                String usIdStr;
                UsID.set(Long.parseLong(queryUserID));
                for (int i = 0; i < lineLength; i++) {
                    usIdStr = usersStrArr[i];
                    if (queryUserID.equals(usIdStr)) {
                        context.write(UsID, usersListValueOut);
                        usIdStr = null;
                    }
                }
            }
        }// end if 2 or more
    }// end map
}// end class