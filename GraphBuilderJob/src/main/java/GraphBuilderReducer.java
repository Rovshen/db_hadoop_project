import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class GraphBuilderReducer extends
        Reducer<LongWritable, Text, LongWritable,
        Text> {
    
    @Override
    public void reduce(LongWritable key, Iterable<Text> val,
            Context
            context)
            throws IOException, InterruptedException {
    
        StringBuilder build = new StringBuilder(";");
        Iterator<Text> values = val.iterator();
        while (values.hasNext()) {
            build.append(values.next().toString()).append(";");
        }
        context.write(key, new Text(build.toString()));
        
    }
}
/**
 * RESULT, where 1A -UsId and 2 is a PrID
 * 1A 2:2B,3A;1:2A,3A;
 * 2A 3:3A;1:1A,3A;
 * 2B 2:1A,3A;
 * 3A 3:2A,3A;2:1A,2B,3A;1:1A,2A,3A;
 */
