import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class UserReducer extends
        Reducer<LongWritable, LongWritable, LongWritable,
        Text> {
    
    @Override
    public void reduce(LongWritable userKey, Iterable<LongWritable> val,
            Context
            context)
            throws IOException, InterruptedException {
    
        Iterator<LongWritable> values = val.iterator();
        StringBuilder build = new StringBuilder();
        Set<Long> uniqueVal = new HashSet<Long>();
        while (values.hasNext()) {
            uniqueVal.add(values.next().get());
        }
        for (Long uniqElem : uniqueVal) {
            build.append(uniqElem.toString()).append(" ");
        }
        
        context.write(userKey, new Text(build.toString()));
    }
}
