import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.MRJobConfig;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class UserIdListJob extends Configured implements Tool {
    
    public static void main(String[] args) throws Exception {
    
        /**
         * hadoop jar UserIdListJob.jar
         * /graphData/inputData/outFileSampleDataSpace.txt
         * /graphData/outputData/folderUserId1 2>&1 > UserList_logfile.txt &
         */
        
        int res = ToolRunner.run(new UserIdListJob(), args);
        System.exit(res);
        
    }
    
    public int run(String[] args) throws Exception {
    
        if (args.length != 2) {
            System.err
                    .println("Usage: hadoop -jar UserIdListJob.jar <input path> "
                            +
                            "<output path>");
            System.exit(1);
        }
        
        Configuration conf = new Configuration();
        conf.set(MRJobConfig.MAP_OUTPUT_KEY_FIELD_SEPERATOR, "\t");
        Job job = Job.getInstance(conf);
        
        job.setJar("UserIdListJob.jar");
        job.setJobName("UserIdListJob");
        
        // Set up the input
        job.setInputFormatClass(KeyValueTextInputFormat.class);
        KeyValueTextInputFormat.addInputPath(job, new Path(args[0]));
        
        // Mapper
        job.setMapperClass(UserMapper.class);
        
        // Reducer
        job.setReducerClass(UserReducer.class);
        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(LongWritable.class);
        // Output
        job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(Text.class);
        
        // Execute
        boolean res = job.waitForCompletion(true);
        if (res) {
            return 0;
        } else {
            return 1;
        }
    }
    
}
