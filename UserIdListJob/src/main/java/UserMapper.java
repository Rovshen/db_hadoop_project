import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class UserMapper extends
        Mapper<Text, Text, LongWritable,
        LongWritable> {
    
    LongWritable keyOut = new LongWritable();
    LongWritable valOut = new LongWritable();
    
    /**
     * Map
     */
    @Override
    public void map(Text key, Text value, Context context)
            throws IOException, InterruptedException {
    
        try {
            keyOut.set(Long.parseLong(key.toString()));
            String[] userRating = value.toString().split(" ");
            String userId = userRating[0];
            valOut.set(Long.parseLong(userId));
            context.write(valOut, keyOut);
        } catch (NumberFormatException e) {
        }
    }// end of map
}
