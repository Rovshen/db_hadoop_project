#!/bin/bash
# request.sh: a shell script to request products suggestion for a given user
	#eval "hadoop fs -cat /graphData/outputData/folderProducId/part-r-00000"
# test hadoop fs -cat /graphData/outputData/folderProducId/part-r-00000
#	eval "hadoop fs -rm /graphData/outputData/folderProducId/part-r-00000"
#	eval "hadoop fs -rm /graphData/outputData/folderProducId/_SUCCESS"
# test hadoop fs -cat /graphData/outputData/folderUserId/part-r-00000
#eval "hadoop fs -cat /graphData/outputData/folderUserId/part-r-00000"

#name of preprocessed data
prepData="part_preprocessed"
prepHDFSfolder="/graphData/inputData/preprocessedData"
inputFolder="/input/"
outputFolder="/output/"
resultFolder="/result/"
usage(){
	echo "Usage: $0 <reviews file name> <user id>"
	#exit 1
}
 
# define is_file_exits function 
# $f -> store argument passed to the script
is_file_exits(){
	local f="$1"	
	[[ -f "$f" ]] && return 0 || return 1
}

#createPrIDList
createPrIDList(){
if ( ! hadoop fs -ls /graphData/outputData/folderProducId11 | grep -q "part-r-00000" )
then
	echo "Starting hadoop ProdId"
	eval "hadoop jar ProductIdListJob.jar $prepHDFSfolder/$prepData /graphData/outputData/folderProducId11 2>&1 > ProductList_logfile.txt &"
	eval "pid=$!"
	eval "wait $pid"
	echo "PrIdListReady and result is"
else
	# Folder exists in the HDFS so do nothing
	echo "Folder folderProducId exists, removing the folder and exiting"
	#clean folder and delete it
	eval "hadoop fs -rm -r /graphData/outputData/folderProducId/"
	#clean folder and delete it
	eval "hadoop fs -rm -r /graphData/outputData/folderUserId/"
	exit 1
fi
}

#createUsIDList
createUsIDList(){
if ( ! hadoop fs -ls /graphData/outputData/folderUserId11 | grep -q "part-r-00000" )
then
	echo "Starting hadoop USerId"
	 eval "hadoop jar UserIdListJob.jar $prepHDFSfolder/$prepData /graphData/outputData/folderUserId11 2>&1 > UserList_logfile.txt &"
	eval "pid=$!"
	eval "wait $pid"
	echo "UsIdListReady and result is"
else
	# Folder exists in the HDFS so do nothing
	echo "Folder folderUserId exists, removing the folder and exiting"
	#clean folder and delete it
	eval "hadoop fs -rm -r /graphData/outputData/folderProducId/"
	#clean folder and delete it
	eval "hadoop fs -rm -r /graphData/outputData/folderUserId/"
	exit 1
fi
}

# invoke  usage
# call usage() function if filename not supplied
[[ $# -ne 2 ]] && usage
 
# Invoke is_file_exits
if ( is_file_exits "$1" )
then
 echo "File found and user id is $2"
else
 echo "File not found"
fi


#exec my_program > $$.log

#start preprocessing if not done already
if ( ! hadoop fs -ls /graphData/inputData/preprocessedData | grep -q $prepData )
then
	#create the file using the preprocessor
	echo "Creating file $prepData"
	eval "java -jar DataPreprocessor.jar $1 $prepData 2>&1 > PreProcess_logfile.txt &"
	eval "pid=$!"
	eval "wait $pid"
	eval "hadoop fs -copyFromLocal $prepData $prepHDFSfolder"
	#echo "Done creating $prepData and starting hadoop"

	eval "start-all.sh"
	eval "pid=$!"
	eval "wait $pid"
	createPrIDList
	createUsIDList
	eval "hadoop jar GraphBuilderJob.jar /graphData/outputData/folderProducId/part-r-00000 /graphData/outputData/folderGraph 2>&1 > Graph_logfile.txt &"	
	eval "stop-all.sh"
	eval "pid=$!"
	eval "wait $pid"
else
	# file exists in the HDFS so do nothing
	echo "File prepData exists"

	eval "start-all.sh"
	eval "pid=$!"
	eval "wait $pid"
	createPrIDList
	createUsIDList	
	eval "hadoop jar GraphBuilderJob.jar /graphData/outputData/folderProducId11/part-r-00000 /graphData/outputData/folderGraph113 > Graph_logfile.txt &"	
	eval "stop-all.sh"
	eval "pid=$!"	
	eval "wait $pid"
fi
