import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class MRIntersectionJob extends Configured implements Tool {
    
    public static void main(String[] args) throws Exception {
    
        int res = ToolRunner.run(new MRIntersectionJob(), args);
        System.exit(res);
        
    }
    
    public int run(String[] args) throws Exception {
    
        if (args.length != 3) {
            System.err
                    .println("Usage: hadoop -jar MRIntersectionJob.jar <Graph 1> <Graph 2><OutputFolder>");
            System.exit(1);
        }
        // Path usIdInputPath = new Path(args[0]);
        Path inputPath1 = new Path(args[0]);
        Path inputPath2 = new Path(args[1]);
        Path outputPath = new Path(args[2]);
        Configuration conf = new Configuration();
        // set the separator
        conf.set("mapreduce.textoutputformat.separator", " ");
        Job job = Job.getInstance(conf);
        
        job.setJar("MRIntersectionJob.jar");
        job.setJobName("MRIntersectionJob");
        
        /**
         * Also a small tip for you, you can separate the files with comma ","
         * so you can set them with a single call like this: hadoop jar
         * capital.jar org.myorg.Capital
         * /user/cloudera/capital/input/City.dat,/user
         * /cloudera/capital/input/Country.dat And in your java
         * code:FileInputFormat.addInputPaths(job, args[1]);
         * */
        
        // Reducer
        job.setReducerClass(MRIntersectionReducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        // Output for reducer
        job.setOutputFormatClass(TextOutputFormat.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        
        // Set up the input
        MultipleInputs.addInputPath(job, inputPath1,
                KeyValueTextInputFormat.class, MRIntersectionMapper.class);
        
        MultipleInputs.addInputPath(job, inputPath2,
                KeyValueTextInputFormat.class,
                MRIntersectionMapper.class);
        // Set up the output
        TextOutputFormat.setOutputPath(job, outputPath);
        
        // Execute
        boolean res = job.waitForCompletion(true);
        if (res) {
            return 0;
        } else {
            return 1;
        }
    }
    
}
