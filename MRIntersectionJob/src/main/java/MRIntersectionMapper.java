import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MRIntersectionMapper extends
        Mapper<Text, Text, Text,
        Text> {
    
    Text keyOut = new Text();
    
    /**
     * Map
     */
    @Override
    public void map(Text keyIn, Text value, Context context)
            throws IOException, InterruptedException {
    
        try {
            String keyStr = value.toString().trim();
            if (!keyStr.equals("")) {
                keyOut.set(keyStr);
                context.write(keyOut, keyIn);
            }
        } catch (NumberFormatException e) {
        }
    }// end of map
}
