import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MRIntersectionReducer extends
        Reducer<Text, Text, Text,
        Text> {
    
    @Override
    public void reduce(Text userKey, Iterable<Text> val,
            Context
            context)
            throws IOException, InterruptedException {
    
        Iterator<Text> values = val.iterator();
        
        int count = 0;
        while (values.hasNext()) {
            count++;
            values.next().toString();
            if (count > 1) {
                context.write(userKey, new Text(""));
                break;
            }
        }
    }
}
