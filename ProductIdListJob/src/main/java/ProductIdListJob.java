import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.MRJobConfig;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class ProductIdListJob extends Configured implements Tool {
    
    public static void main(String[] args) throws Exception {
    
        /**
         * hadoop jar ProductIdListJob.jar
         * /graphData/inputData/outFileSampleDataSpace.txt
         * /graphData/outputData/folderProducId1 2>&1 > ProductList_logfile.txt
         * &
         */
        
        int res = ToolRunner.run(new ProductIdListJob(), args);
        System.exit(res);
        
    }
    
    public int run(String[] args) throws Exception {
    
        if (args.length != 2) {
            System.err
                    .println("Usage: hadoop -jar ProductIdListJob.jar <input path> "
                            +
                            "<output path>");
            System.exit(1);
        }
        
        Configuration conf = new Configuration();
        conf.set(MRJobConfig.MAP_OUTPUT_KEY_FIELD_SEPERATOR, "\t");
        conf.set(
                "mapreduce.input.keyvaluelinerecordreader.key.value.separator",
                "\t");
        Job job = Job.getInstance(conf);
        
        job.setJar("ProductIdListJob.jar");
        job.setJobName("ProductIdListJob");
        
        // Set up the input
        job.setInputFormatClass(KeyValueTextInputFormat.class);
        KeyValueTextInputFormat.addInputPath(job, new Path(args[0]));
        
        // Mapper
        job.setMapperClass(ProductMapper.class);
        
        // Reducer
        job.setReducerClass(ProductReducer.class);
        
        // Output
        job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(LongWritable.class);
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(Text.class);
        
        // Execute
        boolean res = job.waitForCompletion(true);
        if (res) {
            return 0;
        } else {
            return 1;
        }
    }
    
}
