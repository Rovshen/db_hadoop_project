import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MRUnionMapper extends
Mapper<Text, Text, Text,
Text> {

    private final Text keyOut = new Text();

    /**
     * Map
     */
    @Override
    public void map(final Text keyIn, final Text value, final Context context)
            throws IOException, InterruptedException {

        try {
            final String keyStr = value.toString().trim();
            if (!keyStr.equals("")) {
                keyOut.set(keyStr);
                context.write(keyOut, keyIn);
            }
        } catch (final NumberFormatException e) {
        }
    }// end of map

}
